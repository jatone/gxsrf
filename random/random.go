package random

import "crypto/rand"
import "io"

func Bytes(length int) []byte {
	var buffer = make([]byte, length)

	Fill(buffer)

	return buffer
}

func Fill(buffer []byte) {
	FillFromReader(rand.Reader, buffer)
}

func FillFromReader(reader io.Reader, buffer []byte) {
	if _, err := io.ReadFull(reader, buffer); err != nil {
		panic(err)
	}
}
