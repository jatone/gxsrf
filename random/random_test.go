package random_test

import (
	. "bitbucket.org/jatone/gxsrf/random"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	"crypto/subtle"
	"fmt"
)

type errorReader struct{}

func (t errorReader) Read(p []byte) (n int, err error) {
	return 0, fmt.Errorf("I am a reader error")
}

var _ = Describe("Random", func() {
	Context("Bytes", func() {
		It("should generate a byte array of length n", func() {
			Expect(Bytes(10)).To(HaveLen(10))
		})

		It("should generate a different byte sequence each time", func() {
			// 0 indicates the two byte arrays are different.
			Expect(subtle.ConstantTimeCompare(Bytes(10), Bytes(10))).To(Equal(0))
		})
	})

	Context("Fill", func() {
		It("should fill a buffer with random bytes", func() {
			zero_buffer := make([]byte, 10)
			buffer := make([]byte, 10)

			// Assert that the created buffer is equal to the zero buffer.
			Expect(subtle.ConstantTimeCompare(zero_buffer, buffer)).To(Equal(1))

			Fill(buffer)

			// Assert the buffers are now different.
			Expect(subtle.ConstantTimeCompare(zero_buffer, buffer)).To(Equal(0))
		})

		It("should fill with different bytes each time", func() {
			zeroed := make([]byte, 10)
			buffer1 := make([]byte, 10)
			buffer2 := make([]byte, 10)
			Expect(subtle.ConstantTimeCompare(buffer1, buffer2)).To(Equal(1))
			Fill(buffer1)
			Expect(subtle.ConstantTimeCompare(buffer1, buffer2)).To(Equal(0))
			copy(buffer2, buffer1)
			Expect(subtle.ConstantTimeCompare(buffer1, buffer2)).To(Equal(1))
			Fill(buffer1)
			// Assert they no longer match
			Expect(subtle.ConstantTimeCompare(buffer1, buffer2)).To(Equal(0))
			// Assert the buffers are not zerod buffers
			Expect(subtle.ConstantTimeCompare(buffer1, zeroed)).To(Equal(0))
			Expect(subtle.ConstantTimeCompare(buffer2, zeroed)).To(Equal(0))
		})
	})

	Context("FillFromReader", func() {
		It("should panic if the reader throws and error", func() {
			Expect(func() { FillFromReader(errorReader{}, make([]byte, 10)) }).To(Panic())
		})
	})
})
