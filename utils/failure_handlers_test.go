package utils_test

import "bytes"
import "net/http"
import "net/http/httptest"
import "fmt"

import (
	. "bitbucket.org/jatone/gxsrf/utils"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("Failure Handlers", func() {
	var response *httptest.ResponseRecorder

	BeforeEach(func() {
		response = httptest.NewRecorder()
	})

	Context("Forbidden Failure", func() {
		It("should take an error and mark the response as forbidden (403)", func() {
			req, err := http.NewRequest("GET", "http://localhost", bytes.NewReader([]byte{}))
			Expect(err).To(BeNil())

			ForbiddenFailureHandler(fmt.Errorf("an error occurred"), response, req)

			Expect(response.Body.String()).To(Equal("an error occurred\n"))
			Expect(response.Code).To(Equal(http.StatusForbidden))
		})
	})
})
