package utils

import "net/http"

type FailureHandler func(error, http.ResponseWriter, *http.Request)

var ForbiddenFailureHandler = func(e error, w http.ResponseWriter, r *http.Request) {
	http.Error(w, e.Error(), http.StatusForbidden)
}
