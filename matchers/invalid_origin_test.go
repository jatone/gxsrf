package matchers_test

import "bytes"
import "net/http"
import "net/http/httptest"

import (
	. "bitbucket.org/jatone/gxsrf/matchers"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	"bitbucket.org/jatone/gxsrf/utils"
)

var _ = Describe("InvalidOrigin", func() {
	Context("Matches", func() {
		var response *httptest.ResponseRecorder
		invalidOrigin := InvalidOrigin{
			FailureHandler: utils.ForbiddenFailureHandler,
		}

		BeforeEach(func() {
			response = httptest.NewRecorder()
		})

		It("should not match http requests", func() {
			req, err := http.NewRequest("GET", "http://localhost", bytes.NewReader([]byte{}))
			Expect(err).To(BeNil())

			Expect(invalidOrigin.Matches(response, req)).To(BeFalse())
		})

		Context("https request", func() {
			It("should not match a request with the same origin", func() {
				req, err := http.NewRequest("GET", "https://localhost", bytes.NewReader([]byte{}))
				Expect(err).To(BeNil())

				req.Header.Set("Referer", "https://localhost")

				Expect(invalidOrigin.Matches(response, req)).To(BeFalse())
			})

			Context("different origin", func() {
				var req *http.Request
				var match bool

				BeforeEach(func() {
					var err error

					req, err = http.NewRequest("GET", "https://localhost", bytes.NewReader([]byte{}))
					Expect(err).To(BeNil())

					req.Header.Set("Referer", "https://mail.google.com")

					match = invalidOrigin.Matches(response, req)
				})

				It("should match the request", func() {
					Expect(match).To(BeTrue())
				})

				It("should respond with StatusForbidden", func() {
					Expect(response.Code).To(Equal(http.StatusForbidden))
				})

				It("should contain an error message in the body", func() {
					Expect(response.Body.String()).To(Equal("A secure request's Referer comes from a different Origin from the request's URL.\n"))
				})
			})

			Context("with no referer", func() {
				var req *http.Request
				var match bool

				BeforeEach(func() {
					var err error

					req, err = http.NewRequest("GET", "https://localhost", bytes.NewReader([]byte{}))
					Expect(err).To(BeNil())
					Expect(req.Header.Get("Referer")).To(Equal(""))

					match = invalidOrigin.Matches(response, req)
				})

				It("should match a request with no referer", func() {
					Expect(match).To(BeTrue())
				})

				It("should respond with StatusForbidden", func() {
					Expect(response.Code).To(Equal(http.StatusForbidden))
				})

				It("should contain an error message in the body", func() {
					Expect(response.Body.String()).To(Equal("A secure request contained no Referer or its value was malformed.\n"))
				})
			})
		})
	})

	Context("NewInvalidOrigin", func() {
		It("should return a request matcher", func() {
			var matcher RequestMatcher
			matcher = NewInvalidOrigin(utils.ForbiddenFailureHandler, nil)
			Expect(matcher).ToNot(BeNil())
		})
	})
})
