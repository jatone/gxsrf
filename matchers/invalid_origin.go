package matchers

import "net/http"
import "net/url"
import "errors"

import "bitbucket.org/jatone/gxsrf/utils"

var ErrNoReferer = errors.New("A secure request contained no Referer or its value was malformed.")
var ErrBadReferer = errors.New("A secure request's Referer comes from a different Origin from the request's URL.")

type InvalidOrigin struct {
	utils.FailureHandler
}

func (t InvalidOrigin) Matches(w http.ResponseWriter, r *http.Request) bool {
	if r.URL.Scheme == "https" {
		referer, err := url.Parse(r.Header.Get("Referer"))

		// if we can't parse the referer or it's empty,
		// we assume it's not specified
		if err != nil || len(referer.String()) == 0 {
			t.FailureHandler(ErrNoReferer, w, r)
			return true
		}

		if !sameOrigin(referer, r.URL) {
			t.FailureHandler(ErrBadReferer, w, r)
			return true
		}
	}

	return false
}

// Checks if the given URLs have the same origin
// (that is, they share the host, the port and the scheme)
func sameOrigin(u1, u2 *url.URL) bool {
	// we take pointers, as url.Parse() returns a pointer
	// and http.Request.URL is a pointer as well

	// Host is either host or host:port
	return (u1.Scheme == u2.Scheme && u1.Host == u2.Host)
}

func NewInvalidOrigin(failure utils.FailureHandler, next http.Handler) RequestMatcher {
	return InvalidOrigin{
		FailureHandler: failure,
	}
}
