package matchers

import "net/http"

import "bitbucket.org/jatone/gxsrf/utils"

type SafeMethod struct {
	Methods []string
	http.Handler
}

func (t SafeMethod) Matches(w http.ResponseWriter, r *http.Request) bool {
	for _, m := range t.Methods {
		if m == r.Method {
			t.Handler.ServeHTTP(w, r)
			return true
		}
	}

	return false
}

func DefaultSafeMethods(failure utils.FailureHandler, next http.Handler) RequestMatcher {
	return SafeMethod{
		Methods: []string{"GET", "HEAD", "OPTIONS", "TRACE"},
		Handler: next,
	}
}
