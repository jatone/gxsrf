package matchers

import "net/http"

import "bitbucket.org/jatone/gxsrf/utils"

type RequestMatcher interface {
	Matches(http.ResponseWriter, *http.Request) bool
}

type RequestMatcherFactory func(utils.FailureHandler, http.Handler) RequestMatcher
