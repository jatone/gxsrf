package matchers_test

import "bytes"
import "net/http"
import "net/http/httptest"

import (
	. "bitbucket.org/jatone/gxsrf/matchers"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	"bitbucket.org/jatone/gxsrf/utils"
)

var _ = Describe("SafeMethods", func() {
	Context("Matches", func() {
		var response *httptest.ResponseRecorder
		safeMethod := SafeMethod{
			Methods: []string{"GET"},
			Handler: successHandler,
		}

		BeforeEach(func() {
			response = httptest.NewRecorder()
		})

		Context("unsafe method", func() {
			It("should not match", func() {
				req, err := http.NewRequest("POST", "http://localhost", bytes.NewReader([]byte{}))
				Expect(err).To(BeNil())
				Expect(safeMethod.Matches(response, req)).To(BeFalse())
			})
		})

		Context("safe method", func() {
			It("should match", func() {
				req, err := http.NewRequest("GET", "http://localhost", bytes.NewReader([]byte{}))
				Expect(err).To(BeNil())
				Expect(safeMethod.Matches(response, req)).To(BeTrue())
			})
		})
	})

	Context("DefaultSafeMethods", func() {
		It("should specify GET HEAD OPTIONS TRACE as safe methods", func() {
			matcher := DefaultSafeMethods(utils.ForbiddenFailureHandler, nil)
			safemethods, ok := matcher.(SafeMethod)
			Expect(ok).To(BeTrue())
			Expect(safemethods.Methods).To(ConsistOf([]string{"GET", "HEAD", "OPTIONS", "TRACE"}))
		})
	})
})
