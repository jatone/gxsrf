package gxsrf_test

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"net/http"

	"testing"
)

var successHandler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	w.Write([]byte("success"))
})

func TestCsrf(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "gxsrf suite")
}
