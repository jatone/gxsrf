package gxsrf_test

import "bytes"
import "net/http"
import "net/http/httptest"

import (
	. "bitbucket.org/jatone/gxsrf"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	"bitbucket.org/jatone/gxsrf/matchers"
	"bitbucket.org/jatone/gxsrf/storage"
	"bitbucket.org/jatone/gxsrf/tokens"
	"bitbucket.org/jatone/gxsrf/utils"
)

func DefaultConstantXSRF() Config {
	return Config{
		CookieToken:  DefaultCookieToken,
		Form:         "xsrf",
		Header:       "X-XSRF-TOKEN",
		TokenStorage: storage.NewDefaultConstant(tokens.NewSHA256(64)),
		ShortCircuit: []matchers.RequestMatcherFactory{
			matchers.DefaultSafeMethods,
			matchers.NewInvalidOrigin,
		},
		FailureHandler: utils.ForbiddenFailureHandler,
	}
}

var _ = Describe("Csrf", func() {
	Context("DefaultSessionXSRF", func() {
		It("should return a gxsrf.Config", func() {
			config := DefaultSessionXSRF()
			failurehandler := utils.ForbiddenFailureHandler

			Expect(config.Form).To(Equal("xsrf"))
			Expect(config.Header).To(Equal("X-XSRF-TOKEN"))
			Expect(config.TokenStorage).To(BeAssignableToTypeOf(storage.NewDefaultSession()))
			Expect(config.ShortCircuit).To(HaveLen(2))
			// TODO figure out how to get this to work.
			// Expect(config.ShortCircuit).To(ContainElement(gxsrf.RequestMatcherFactory(gxsrf.DefaultSafeMethodMatcher)))
			Expect(config.FailureHandler).To(BeAssignableToTypeOf(failurehandler))
		})
	})

	Context("Config", func() {
		It("should build a csrf http handler", func() {
			token_storage := storage.NewDefaultConstant(tokens.Constant("abc123"))
			config := Config{
				CookieToken:  DefaultCookieToken,
				Form:         "xsrf",
				Header:       "X-XSRF-TOKEN",
				TokenStorage: token_storage,
				ShortCircuit: []matchers.RequestMatcherFactory{
					matchers.DefaultSafeMethods,
					matchers.NewInvalidOrigin,
				},
				FailureHandler: utils.ForbiddenFailureHandler,
			}

			handler := config.CheckToken(successHandler)
			csrfHandler, ok := handler.(Csrf)
			Expect(ok).To(BeTrue())
			Expect(csrfHandler.CookieToken).To(Equal(DefaultCookieToken))
			Expect(csrfHandler.Authenticator.Form).To(Equal("xsrf"))
			Expect(csrfHandler.Authenticator.Header).To(Equal("X-XSRF-TOKEN"))
			Expect(csrfHandler.TokenStorage).To(Equal(token_storage))
		})
	})

	Context("csrf handler", func() {
		It("should short circuit if a RequestMatcher triggers", func() {
			config := DefaultSessionXSRF()

			response := httptest.NewRecorder()
			// Get Requests trigger the SafeMethod short circuit
			req, err := http.NewRequest("GET", "http://localhost", bytes.NewReader([]byte{}))
			Expect(err).To(BeNil())

			handler := config.CheckToken(successHandler)

			handler.ServeHTTP(response, req)
			Expect(response.Code).To(Equal(http.StatusOK))
		})

		It("should authenticate a token on the request", func() {
			token := tokens.NewSHA256(32)
			config := DefaultConstantXSRF()
			config.TokenStorage = storage.NewDefaultConstant(token)
			// no short circuits
			config.ShortCircuit = []matchers.RequestMatcherFactory{}

			response := httptest.NewRecorder()
			req, err := http.NewRequest("GET", "http://localhost", bytes.NewReader([]byte{}))
			Expect(err).To(BeNil())

			// Add token to header
			req.Header.Set(config.Header, token.Generate())

			handler := config.CheckToken(successHandler)

			handler.ServeHTTP(response, req)
			Expect(response.Code).To(Equal(http.StatusOK))
		})

		It("should reject any requests with a bad token", func() {
			// bad token
			token := tokens.NewSHA256(32)

			config := DefaultConstantXSRF()
			// no short circuits
			config.ShortCircuit = []matchers.RequestMatcherFactory{}

			response := httptest.NewRecorder()
			req, err := http.NewRequest("GET", "http://localhost", bytes.NewReader([]byte{}))
			Expect(err).To(BeNil())

			// Add token to header
			req.Header.Set(config.Header, token.Generate())

			handler := config.CheckToken(successHandler)

			handler.ServeHTTP(response, req)
			Expect(response.Code).To(Equal(http.StatusForbidden))
		})

		It("should reject requests with no tokens", func() {
			config := DefaultConstantXSRF()
			// no short circuits
			config.ShortCircuit = []matchers.RequestMatcherFactory{}

			response := httptest.NewRecorder()
			req, err := http.NewRequest("GET", "http://localhost", bytes.NewReader([]byte{}))
			Expect(err).To(BeNil())

			handler := config.CheckToken(successHandler)

			handler.ServeHTTP(response, req)
			Expect(response.Code).To(Equal(http.StatusForbidden))
		})

		It("should generate a token on the response", func() {
			config := DefaultConstantXSRF()

			response := httptest.NewRecorder()
			req, err := http.NewRequest("GET", "http://localhost", bytes.NewReader([]byte{}))
			Expect(err).To(BeNil())

			handler := config.CheckToken(successHandler)

			handler.ServeHTTP(response, req)

			cookies := (&http.Response{Header: response.Header()}).Cookies()
			Expect(cookies).To(HaveLen(1))
			Expect(cookies[0].Name).To(Equal(config.CookieToken.Name))
		})
	})
})
