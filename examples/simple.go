// Demonstrates simple usage
// +build ignore
package main

import "fmt"
import "html/template"
import "net/http"

import "bitbucket.org/jatone/gxsrf"

var templateString = `
<!doctype html>
<html>
<body>
{{ if .name }}
<p>Your name: {{ .name }}</p>
{{ end }}
<form action="/" method="POST">
<input type="text" name="name">

<!-- change the value -->
<input type="hidden" name="xsrf" value="{{XSRFToken}}">
<input type="submit" value="Send">
</form>
</body>
</html>
`

// stub up functions that will be provided to the template.
var funcMap = template.FuncMap{
	"XSRFToken": func() string { return "" },
}

var templ = template.Must(template.New("t1").Funcs(funcMap).Parse(templateString))

func myFunc(token_storage gxsrf.TokenStorage) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		context := make(map[string]string)

		if r.Method == "POST" {
			context["name"] = r.FormValue("name")
		}

		// Generally you would provide a renderer that would provide this function to your templates
		// for the request.
		templ.Funcs(template.FuncMap{
			"XSRFToken": XSRFToken(token_storage, w, r),
		}).Execute(w, context)
	}
}

func XSRFToken(t gxsrf.TokenStorage, w http.ResponseWriter, r *http.Request) func() string {
	var token gxsrf.Token
	var err error

	if token, err = t.Load(r); err != nil {
		// if an error occurred when loading, get generate a useless token
		return func() string { return "" }
	}

	return func() string { return token.Generate() }
}

func main() {
	// Generate a default session XSRF configuration
	xsrf := gxsrf.DefaultSessionXSRF()
	// Wrap your handler with the XSRF Handler.
	myHandler := xsrf.CheckToken(http.HandlerFunc(myFunc(xsrf)))
	fmt.Println("Listening on http://127.0.0.1:8000/")
	http.ListenAndServe(":8000", myHandler)
}
