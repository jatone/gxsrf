package gxsrf_test

import "bytes"
import "net/http"
import "net/http/httptest"
import "net/url"
import "strings"

import (
	. "bitbucket.org/jatone/gxsrf"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	"bitbucket.org/jatone/gxsrf/tokens"
	"bitbucket.org/jatone/gxsrf/utils"
)

var _ = Describe("Authenticator", func() {
	token := tokens.Constant("abc123")
	authenticator := Authenticator{
		Form:           "xsrf",
		Header:         "X-XSRF-TOKEN",
		Handler:        successHandler,
		FailureHandler: utils.ForbiddenFailureHandler,
	}

	Context("Authenticate", func() {
		It("should authenticate tokens provided in the specified header field", func() {
			recorder := httptest.NewRecorder()
			req, err := http.NewRequest("GET", "http://localhost", bytes.NewReader([]byte{}))
			Expect(err).To(BeNil())

			req.Header.Set(authenticator.Header, "abc123")

			authenticator.Authenticate(token, recorder, req)
			Expect(recorder.Code).To(Equal(http.StatusOK))
		})

		It("should authenticate tokens provided in the specified form field", func() {
			recorder := httptest.NewRecorder()
			form := url.Values{}
			form.Add(authenticator.Form, "abc123")
			req, err := http.NewRequest("GET", "http://localhost", strings.NewReader(form.Encode()))
			Expect(err).To(BeNil())

			req.Header.Set(authenticator.Header, "abc123")

			authenticator.Authenticate(token, recorder, req)
			Expect(recorder.Code).To(Equal(http.StatusOK))
		})

		It("should not authenticate if there are no tokens", func() {
			recorder := httptest.NewRecorder()
			req, err := http.NewRequest("GET", "http://localhost", bytes.NewReader([]byte{}))
			Expect(err).To(BeNil())

			authenticator.Authenticate(token, recorder, req)
			Expect(recorder.Code).To(Equal(http.StatusForbidden))
		})

		It("should not authenticate if the token is invalid", func() {
			recorder := httptest.NewRecorder()
			req, err := http.NewRequest("GET", "http://localhost", bytes.NewReader([]byte{}))
			Expect(err).To(BeNil())

			req.Header.Set(authenticator.Header, "abc1234")

			authenticator.Authenticate(token, recorder, req)
			Expect(recorder.Code).To(Equal(http.StatusForbidden))
		})
	})
})
