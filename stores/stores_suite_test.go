package stores_test

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	"testing"
)

func TestStores(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Stores Suite")
}
