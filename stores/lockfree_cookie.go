package stores

import "net/http"
import "time"

import "github.com/gorilla/securecookie"
import "github.com/gorilla/sessions"

const PREFIX = "LOCKLESS-COOKIE-NEW-"

func DefaultCookie() http.Cookie {
	return http.Cookie{
		Path:     "/",
		MaxAge:   int(time.Hour),
		HttpOnly: true,
	}
}

// EXPERIMENTAL Session Storage
// LockfreeCookie ----------------------------------------------------------------
func DefaultLockfreeCookie() *LockfreeCookie {
	return NewLockfreeCookie(securecookie.GenerateRandomKey(32), securecookie.GenerateRandomKey(32))
}

// NewLockfreeCookie returns a new LockfreeCookie.
//
// Keys are defined in pairs to allow key rotation, but the common case is
// to set a single authentication key and optionally an encryption key.
//
// The first key in a pair is used for authentication and the second for
// encryption. The encryption key can be set to nil or omitted in the last
// pair, but the authentication key is required in all pairs.
//
// It is recommended to use an authentication key with 32 or 64 bytes.
// The encryption key, if set, must be either 16, 24, or 32 bytes to select
// AES-128, AES-192, or AES-256 modes.
//
// Use the convenience function securecookie.GenerateRandomKey() to create
// strong keys.
func NewLockfreeCookie(keyPairs ...[]byte) *LockfreeCookie {
	return &LockfreeCookie{
		Codecs:         securecookie.CodecsFromPairs(keyPairs...),
		CookieTemplate: DefaultCookie(),
	}
}

// LockfreeCookie stores sessions using secure cookies.
type LockfreeCookie struct {
	Codecs         []securecookie.Codec
	CookieTemplate http.Cookie
}

// Get see New
func (t LockfreeCookie) Get(r *http.Request, name string) (*sessions.Session, error) {
	return t.New(r, name)
}

// New returns a session for the given name.
//
// It returns a new session if the session doesn't exist. Access IsNew on
// the session to check if it is an existing session or a new one.
//
// It returns a new session and an error if the session exists but could
// not be decoded.
func (t LockfreeCookie) New(r *http.Request, name string) (*sessions.Session, error) {
	var session *sessions.Session
	var err error

	// The new cookie should take precedence over the old cookie.
	// We store the current token value inside the request as
	// a cookie with a special name. Makes for a easy lookup
	// without locking being required.
	if session, err = t.lookup(newSessionName(name), r); err == nil {
		return session, nil
	}

	if session, err = t.lookup(name, r); err == nil {
		return session, nil
	}

	session = sessions.NewSession(t, name)
	session.Options = options(t.CookieTemplate)

	return session, err
}

// Save adds a single session to the response.
func (t LockfreeCookie) Save(r *http.Request, w http.ResponseWriter, session *sessions.Session) error {
	var encoded, new_encoded string
	var err error
	new_session_name := newSessionName(session.Name())

	encoded, err = t.encode(session.Name(), session)
	new_encoded, err = t.encode(new_session_name, session)

	if err != nil {
		return err
	}

	http.SetCookie(w, sessions.NewCookie(session.Name(), encoded, session.Options))

	// Lets avoid locking unnecessarily to lookup the token.
	// we'll munge the request with a special cookie that has the new
	// token being stored.
	// Also set the cookie on the request for thread safe lookup
	// give it a different name so it doesn't clash with the original request token.
	r.AddCookie(sessions.NewCookie(new_session_name, new_encoded, session.Options))
	return nil
}

// Lookup a token by name if the token cannot be found
// return nil.
func (t LockfreeCookie) lookup(name string, r *http.Request) (*sessions.Session, error) {
	var cookie *http.Cookie
	var err error
	session := sessions.NewSession(t, name)

	session.Options = options(t.CookieTemplate)

	session.IsNew = true

	if cookie, err = r.Cookie(name); err != nil {
		return session, err
	}

	if err = securecookie.DecodeMulti(name, cookie.Value, &session.Values, t.Codecs...); err == nil {
		session.IsNew = false
	}

	return session, err
}

func (t LockfreeCookie) encode(name string, session *sessions.Session) (string, error) {
	return securecookie.EncodeMulti(name, session.Values, t.Codecs...)
}

func options(cookie http.Cookie) *sessions.Options {
	return &sessions.Options{
		Path:     cookie.Path,
		Domain:   cookie.Domain,
		MaxAge:   cookie.MaxAge,
		Secure:   cookie.Secure,
		HttpOnly: cookie.HttpOnly,
	}
}

func newSessionName(name string) string {
	return PREFIX + name
}
