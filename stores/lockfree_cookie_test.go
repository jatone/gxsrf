package stores_test

import (
	. "bitbucket.org/jatone/gxsrf/stores"

	"bytes"
	"strings"
	"net/http"
	"net/http/httptest"

	"github.com/gorilla/securecookie"
	"github.com/gorilla/sessions"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("LockfreeCookie", func() {
	Context("New", func() {
		It("should return the cookie from the request", func() {
			name := "mysession"
			store := DefaultLockfreeCookie()
			session1 := sessions.NewSession(store, name)
			session1.Values["foo"] = "bar"
			encoded, err := securecookie.EncodeMulti(name, session1.Values, store.Codecs...)
			Expect(err).To(BeNil())

			req, err := http.NewRequest("GET", "http://localhost", bytes.NewReader([]byte{}))
			Expect(err).To(BeNil())

			cookie := http.Cookie{
				Name:  name,
				Value: encoded,
			}

			req.AddCookie(&cookie)

			session2, err := store.New(req, name)
			Expect(err).To(BeNil())
			Expect(session1.Values["foo"]).To(Equal(session2.Values["foo"]))
		})
	})

	Context("Get", func() {
		It("should delegate to New", func() {
			req, err := http.NewRequest("GET", "http://localhost", bytes.NewReader([]byte{}))

			Expect(err).To(BeNil())

			store := DefaultLockfreeCookie()
			// ignore the named cookie not present error.
			session, _ := store.Get(req, "cookie")

			Expect(session).ToNot(BeNil())
		})
	})

	Context("Save", func() {
		It("should return an error when something does wrong during encoding", func() {
			recorder := httptest.NewRecorder()
			req, err := http.NewRequest("GET", "http://localhost", bytes.NewReader([]byte{}))

			Expect(err).To(BeNil())

			store := DefaultLockfreeCookie()

			// ignore the named cookie not present error.
			session, _ := store.Get(req, "cookie")

			session.Values["blah"] = "blah"

			store.Codecs = []securecookie.Codec{}
			err = store.Save(req, recorder, session)

			Expect(err).ToNot(BeNil())
			Expect(err.Error()).To(Equal("securecookie: no codecs provided"))
		})

		It("should be able to decode a saved session", func() {
			recorder := httptest.NewRecorder()
			req, err := http.NewRequest("GET", "http://localhost", bytes.NewReader([]byte{}))

			Expect(err).To(BeNil())

			Expect(len(recorder.Header()[http.CanonicalHeaderKey("Set-Cookie")])).To(Equal(0))

			store := DefaultLockfreeCookie()

			session, err := store.Get(req, "cookie")
			Expect(err.Error()).To(Equal("http: named cookie not present"))

			err = store.Save(req, recorder, session)
			Expect(err).To(BeNil())

			session, err = store.Get(req, "cookie")

			Expect(err).To(BeNil())
			Expect(session.Values["blah"])
		})

		It("should add the token cookie to the response", func() {
			recorder := httptest.NewRecorder()
			req, err := http.NewRequest("GET", "http://localhost", bytes.NewReader([]byte{}))

			Expect(err).To(BeNil())

			Expect(len(recorder.Header()[http.CanonicalHeaderKey("Set-Cookie")])).To(Equal(0))

			store := DefaultLockfreeCookie()
			// ignore the named cookie not present error.
			session, err := store.Get(req, "cookie")
			Expect(err.Error()).To(Equal("http: named cookie not present"))

			session.Values["blah"] = "blah"

			err = store.Save(req, recorder, session)

			Expect(err).To(BeNil())
			Expect(len(recorder.Header()[http.CanonicalHeaderKey("Set-Cookie")])).To(Equal(1))
		})

		It("should add the new token cookie to the request", func() {
			recorder := httptest.NewRecorder()
			req, err := http.NewRequest("GET", "http://localhost", bytes.NewReader([]byte{}))

			Expect(err).To(BeNil())

			store := DefaultLockfreeCookie()
			// ignore the named cookie not present error.
			session, _ := store.New(req, store.CookieTemplate.Name)

			session.Values["blah"] = "blah"

			err = store.Save(req, recorder, session)
			Expect(err).To(BeNil())

			cookies := req.Cookies()

			Expect(len(cookies)).To(Equal(1))

			// strip base64 padding...
			Expect(cookies[0].Name).To(Equal(strings.Trim(PREFIX+store.CookieTemplate.Name, "=")))
		})
	})
})
