package gxsrf

import "net/http"

import "bitbucket.org/jatone/gxsrf/tokens"
import "bitbucket.org/jatone/gxsrf/utils"

// An Authenticator is responsible for pulling the token
// out of the given http request.
type Authenticator struct {
	// Names for Header and Form fields to check
	Header, Form string

	http.Handler
	utils.FailureHandler
}

func (t Authenticator) Authenticate(token tokens.Token, w http.ResponseWriter, r *http.Request) {
	if token.Authenticate(r.Header.Get(t.Header)) || token.Authenticate(r.PostFormValue(t.Form)) {
		t.Handler.ServeHTTP(w, r)
	} else {
		t.FailureHandler(tokens.ErrBadToken, w, r)
	}
}
