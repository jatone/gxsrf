[![Build Status](https://drone.io/bitbucket.org/jatone/gxsrf/status.png)](https://drone.io/bitbucket.org/jatone/gxsrf/latest)

### Overview ###

GXSRF is a flexible library for preventing XSRF attacks on a web application for golang.

[example usage](https://bitbucket.org/jatone/gxsrf/src/master/examples/simple.go)

it is split into a few pieces to promote flexibility on the backend.

* tokens - the tokens themselves, based around an interface so new ways of generating tokens are easy to add.
* storage - storage mechanism (default is session)
* stores   - Backends to the session storage (currently supports file, cookie, and lockfree cookie storage backends) Can use 3rd party backends like [redis](https://github.com/boj/redistore)
* csrf - pulls the three other pieces together to prevent XSRF attacks.

```go
// Default implementation is a SHA256 token of a secret and a random set of bytes
type Token interface {
	// Generates a token
	// the result is a hex encoded []byte
	// that can be checked by the Authenticate
	// method.
	Generate() string

	// Authenticate should take in the encoded token
	// and determine if it is a valid value.
	Authenticate(string) bool
}

// Implement this interface for new ways of storing tokens.
type TokenStorage interface {
	// load the token from the request.
	Load(*http.Request) (Token, error)

	// replaces the current token
	// with a newly generated token.
	// if for some reason it is impossible
	// to generate a token it should panic.
	NewToken(http.ResponseWriter, *http.Request) Token
}
```

### Installation ###
```bash
# Testing Packages
go get -t github.com/onsi/ginkgo/ginkgo
go get -t github.com/onsi/gomega

# Dependencies
# These dependencies are used by the session TokenStorage implementation
# which may be moved to its own repository someday.
go get github.com/gorilla/securecookie
go get github.com/gorilla/sessions

# Execute tests
ginkgo -r
```