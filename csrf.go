package gxsrf

import "net/http"
import "time"

import "bitbucket.org/jatone/gxsrf/tokens"
import "bitbucket.org/jatone/gxsrf/utils"
import "bitbucket.org/jatone/gxsrf/matchers"
import "bitbucket.org/jatone/gxsrf/storage"

// Expose interfaces to the world
type Token tokens.Token
type TokenStorage tokens.TokenStorage

var DefaultCookieToken = http.Cookie{
	Name: "XSRF-TOKEN",
	Path: "/",
	MaxAge: int((24 * time.Hour) / time.Second),
}

func DefaultSessionXSRF() Config {
	return Config{
		CookieToken:  DefaultCookieToken,
		Form:         "xsrf",
		Header:       "X-XSRF-TOKEN",
		TokenStorage: storage.NewDefaultSession(),
		ShortCircuit: []matchers.RequestMatcherFactory{
			matchers.DefaultSafeMethods,
			matchers.NewInvalidOrigin,
		},
		FailureHandler: utils.ForbiddenFailureHandler,
	}
}

type Config struct {
	// Names for Header and Form fields to check
	Header, Form string

	// CSRF cookie token for use by javascript libraries.
	CookieToken http.Cookie

	// responsible for storing a token safely
	// and generating new tokens.
	tokens.TokenStorage

	// Matchers that short circuit the request before
	// we check the token.
	ShortCircuit []matchers.RequestMatcherFactory

	// Handler for failures
	utils.FailureHandler
}

func (t Config) CheckToken(next http.Handler) http.Handler {
	matchers := make([]matchers.RequestMatcher, 0, len(t.ShortCircuit))
	for _, f := range t.ShortCircuit {
		matchers = append(matchers, f(t.FailureHandler, next))
	}

	return Csrf{
		CookieToken:  t.CookieToken,
		TokenStorage: t.TokenStorage,
		ShortCircuit: matchers,
		Authenticator: Authenticator{
			Header:         t.Header,
			Form:           t.Form,
			FailureHandler: t.FailureHandler,
			Handler:        next,
		},
	}
}

type Csrf struct {
	CookieToken http.Cookie
	tokens.TokenStorage
	ShortCircuit []matchers.RequestMatcher
	Authenticator
}

func (t Csrf) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var token tokens.Token
	var err error

	if token, err = t.TokenStorage.Load(r); err != nil {
		// Token missing generate new token.
		token = t.regenerateToken(w, r)
	}

	t.regenerateToken(w, r)

	for _, m := range t.ShortCircuit {
		// If the request matchers
		// then the matcher should handle
		// the request and this method should return.
		if m.Matches(w, r) {
			return
		}
	}

	t.Authenticator.Authenticate(token, w, r)
}

func (t Csrf) regenerateToken(w http.ResponseWriter, r *http.Request) tokens.Token {
	token := t.TokenStorage.NewToken(w, r)
	// update the js readable session cookie
	cookie := t.CookieToken
	cookie.Value = token.Generate()
	http.SetCookie(w, &cookie)

	return token
}
