go get -t github.com/onsi/ginkgo/ginkgo
go get -t github.com/onsi/gomega
go get -t github.com/smartystreets/goconvey

go get github.com/gorilla/securecookie
go get github.com/gorilla/sessions

go fmt bitbucket.org/jatone/gxsrf/...
go build bitbucket.org/jatone/gxsrf/...
go install examples/simple.go
