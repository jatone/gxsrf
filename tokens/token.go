package tokens

import "errors"
import "net/http"
import "encoding/base64"

var ErrBadToken = errors.New("Invalid CSRF token received in a form/header.")
var ErrMissingToken = errors.New("The CSRF token cannot be found.")

type TokenFactory func(length int) Token

type Token interface {
	// Generates a token
	// the result is a hex encoded []byte
	// that can be checked by the Authenticate
	// method.
	Generate() string

	// Authenticate should take in the encoded token
	// and determine if it is a valid value.
	Authenticate(string) bool
}

type TokenStorage interface {
	// load the token from the request.
	Load(*http.Request) (Token, error)

	// replaces the current token
	// with a newly generated token.
	// if for some reason it is impossible
	// to generate a token it should panic.
	NewToken(http.ResponseWriter, *http.Request) Token
}

func DecodeToken(h string) ([]byte, error) {
	return base64.URLEncoding.DecodeString(h)
}

func EncodeToken(value []byte) string {
	return base64.URLEncoding.EncodeToString(value)
}
