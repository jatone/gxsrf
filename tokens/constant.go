package tokens

// Basic token for testing and debugging purposes.
type Constant string

func (t Constant) Generate() string {
	return string(t)
}

func (t Constant) Authenticate(token string) bool {
	return token == string(t)
}
