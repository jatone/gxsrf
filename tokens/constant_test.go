package tokens_test

import (
	. "bitbucket.org/jatone/gxsrf/tokens"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("Constant", func() {
	It("should be able to convert a string to a Constant Token", func() {
		token := Token(Constant("abc123"))
		Expect(Constant("abc123")).To(BeAssignableToTypeOf(token))
	})

	It("should return the same token every time", func() {
		token := Constant("abc123")
		Expect(token.Generate()).To(Equal("abc123"))
		Expect(token.Generate()).To(Equal("abc123"))
	})

	It("should authenticate an equivalent constant token", func() {
		token := Constant("abc123")
		Expect(token.Authenticate("abc123")).To(BeTrue())
	})
})
