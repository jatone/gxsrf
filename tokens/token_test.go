package tokens_test

import (
	. "bitbucket.org/jatone/gxsrf/tokens"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("Token", func() {
	raw := "abc123"
	// precomputed
	b64 := "YWJjMTIz"

	Context("EncodeToken", func() {
		It("should base64 encode the byte array", func() {
			result := EncodeToken([]byte(raw))
			Expect(result).To(Equal(b64))
		})
	})

	Context("DecodeToken", func() {
		It("should decode a base64 string into a byte array", func() {
			result, err := DecodeToken(b64)
			Expect(err).To(BeNil())
			Expect(result).To(Equal([]byte(raw)))
		})
	})
})
