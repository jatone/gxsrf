package tokens_test

import (
	. "bitbucket.org/jatone/gxsrf/tokens"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	"bitbucket.org/jatone/gxsrf/random"
	"math/rand"
)

var _ = Describe("Sha256", func() {
	Context("Generate", func() {
		It("should generate a new token on each invocation", func() {
			t1 := NewSHA256(32)
			Expect(t1.Generate()).ToNot(Equal(t1.Generate()))
		})
	})

	Context("Authenticate", func() {
		It("should return true for tokens from the Generate method", func() {
			t1 := NewSHA256(32)
			Expect(t1.Authenticate(t1.Generate())).To(BeTrue())
		})

		It("should return false for empty tokens", func() {
			t1 := NewSHA256(32)
			Expect(t1.Authenticate("")).To(BeFalse())
		})

		It("should return false for odd length tokens", func() {
			t1 := NewSHA256(32)

			oddlength := string(random.Bytes(5))

			Expect(t1.Authenticate(oddlength)).To(BeFalse())
		})

		It("should return false when token is less than expected", func() {
			t1 := NewSHA256(32)

			small := EncodeToken(random.Bytes(12))

			Expect(t1.Authenticate(small)).To(BeFalse())
		})

		It("should return false when token is greater than expected", func() {
			t1 := NewSHA256(32)

			large := EncodeToken(random.Bytes(128))

			Expect(t1.Authenticate(large)).To(BeFalse())
		})

		Context("tampered tokens", func() {
			It("should return false if the a byte has been tampered with", func() {
				t1 := NewSHA256(32)

				tampered, err := DecodeToken(t1.Generate())

				Expect(err).To(BeNil())

				// select a random byte to flip.
				index := rand.Intn(len(tampered) - 1)

				// flip some bits
				tampered[index] = tampered[index] ^ 0x11

				// reencode the token
				badtoken := EncodeToken(tampered)

				Expect(t1.Authenticate(badtoken)).To(BeFalse())
			})
		})
	})
})
