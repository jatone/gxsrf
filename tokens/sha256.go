package tokens

import "crypto/sha256"
import "crypto/subtle"
import "encoding/gob"

import "bitbucket.org/jatone/gxsrf/random"

// register the type for gob encoding
func init() {
	gob.Register(SHA256{})
}

func NewSHA256(length int) Token {
	return SHA256{
		Length: length,
		Secret: random.Bytes(length),
	}
}

type SHA256 struct {
	// Length of the one time token
	Length int

	// Secret known only to token storage
	Secret []byte
}

func (t SHA256) Generate() string {
	buffer := make([]byte, t.Length+sha256.Size)

	random.Fill(buffer[:t.Length])

	copy(buffer[t.Length:], t.hash(t.Secret, buffer[:t.Length]))

	return EncodeToken(buffer)
}

func (t SHA256) Authenticate(token string) bool {
	var buffer []byte
	var err error

	// if we have trouble decoding the token return false.
	if buffer, err = DecodeToken(token); err != nil {
		return false
	}

	// if buffer isn't the proper size then the token is bad.
	if len(buffer) != (t.Length + sha256.Size) {
		return false
	}

	signature := buffer[t.Length:]
	computed := t.hash(t.Secret, buffer[:t.Length])

	return subtle.ConstantTimeCompare(signature, computed) == 1
}

func (t SHA256) hash(chunks ...[]byte) []byte {
	hash := sha256.New()

	for _, chunk := range chunks {
		hash.Write(chunk)
	}

	return hash.Sum(nil)
}
