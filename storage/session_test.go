package storage_test

import (
	. "bitbucket.org/jatone/gxsrf/storage"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	"bytes"
	"fmt"
	"net/http"
	"net/http/httptest"

	"github.com/gorilla/sessions"

	"bitbucket.org/jatone/gxsrf/tokens"
)

var badStoreError = fmt.Errorf("store error")

type badStore struct{}

func (t badStore) Get(r *http.Request, name string) (*sessions.Session, error) {
	return sessions.NewSession(t, name), badStoreError
}

func (t badStore) New(r *http.Request, name string) (*sessions.Session, error) {
	return sessions.NewSession(t, name), badStoreError
}

func (t badStore) Save(r *http.Request, w http.ResponseWriter, s *sessions.Session) error {
	return badStoreError
}

var _ = Describe("Session", func() {
	storage := NewDefaultSession()

	Context("NewToken", func() {
		It("should add the token cookie to the session", func() {
			recorder := httptest.NewRecorder()
			req, err := http.NewRequest("GET", "http://localhost", bytes.NewReader([]byte{}))

			Expect(err).To(BeNil())

			token := storage.NewToken(recorder, req)

			Expect(token).NotTo(BeNil())

			token2, err := storage.Load(req)

			Expect(err).To(BeNil())
			Expect(token).To(Equal(token2))
		})

		It("should still return a token even if the session fails to save", func() {
			storage := NewSession(32, "XSRF-SESSION", tokens.NewSHA256, badStore{})
			recorder := httptest.NewRecorder()
			req, err := http.NewRequest("GET", "http://localhost", bytes.NewReader([]byte{}))

			Expect(err).To(BeNil())

			token := storage.NewToken(recorder, req)

			Expect(token).ToNot(BeNil())
		})
	})

	Context("Load", func() {
		It("should return a ErrMissingToken when the token is not found", func() {
			req, err := http.NewRequest("GET", "http://localhost", bytes.NewReader([]byte{}))

			Expect(err).To(BeNil())

			token, err := storage.Load(req)

			Expect(token).To(BeNil())
			Expect(err).To(MatchError(tokens.ErrMissingToken))
		})

		// 	It("should return a token when the request has a cookie", func() {
		// 		cookie := storage.CookieTemplate
		// 		token := storage.Factory(storage.Length)

		// 		err := storage.CookieCodec.Encode(&cookie, token)
		// 		Expect(err).To(BeNil())

		// 		req, err := http.NewRequest("GET", "http://localhost", bytes.NewReader([]byte{}))
		// 		Expect(err).To(BeNil())
		// 		req.AddCookie(&cookie)

		// 		actualToken, err := storage.Load(req)
		// 		Expect(err).To(BeNil())

		// 		Expect(actualToken).To(Equal(token))
		// 	})

		// 	It("should return a token when the request has a new token cookie", func() {
		// 		cookie := storage.CookieTemplate
		// 		cookie.Name = storage.NewTokenName
		// 		token := storage.Factory(storage.Length)

		// 		err := storage.CookieCodec.Encode(&cookie, token)
		// 		Expect(err).To(BeNil())

		// 		req, err := http.NewRequest("GET", "http://localhost", bytes.NewReader([]byte{}))
		// 		Expect(err).To(BeNil())
		// 		req.AddCookie(&cookie)

		// 		actualToken, err := storage.Load(req)
		// 		Expect(err).To(BeNil())

		// 		Expect(actualToken).To(Equal(token))
		// 	})
	})
})
