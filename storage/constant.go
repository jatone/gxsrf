package storage

import "net/http"

import "bitbucket.org/jatone/gxsrf/tokens"

func NewDefaultConstant(t tokens.Token) Constant {
	return Constant{Token: t}
}

type Constant struct {
	tokens.Token
}

func (t Constant) Load(r *http.Request) (tokens.Token, error) {
	return t.Token, nil
}

func (t Constant) NewToken(w http.ResponseWriter, r *http.Request) tokens.Token {
	return t.Token
}
