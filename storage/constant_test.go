package storage_test

import (
	. "bitbucket.org/jatone/gxsrf/storage"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	"bytes"
	"net/http"
	"net/http/httptest"

	"bitbucket.org/jatone/gxsrf/tokens"
)

var _ = Describe("Constant", func() {
	storage := NewDefaultConstant(tokens.NewSHA256(64))

	Context("NewToken", func() {
		It("should return the same token", func() {
			recorder := httptest.NewRecorder()
			req, err := http.NewRequest("GET", "http://localhost", bytes.NewReader([]byte{}))
			Expect(err).To(BeNil())

			token := storage.NewToken(recorder, req)

			Expect(storage.Token).To(Equal(token))
		})
	})
	Context("Load", func() {
		It("should return the same token", func() {
			req, err := http.NewRequest("GET", "http://localhost", bytes.NewReader([]byte{}))
			Expect(err).To(BeNil())

			token, err := storage.Load(req)
			Expect(err).To(BeNil())
			Expect(storage.Token).To(Equal(token))
		})
	})
})
