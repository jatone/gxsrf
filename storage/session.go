package storage

import "net/http"

import "github.com/gorilla/sessions"

import "bitbucket.org/jatone/gxsrf/tokens"
import "bitbucket.org/jatone/gxsrf/random"

const (
	TOKEN_NAME = "token"
)

func NewDefaultSession() tokens.TokenStorage {
	return NewSession(32, "XSRF-SESSION", tokens.NewSHA256, sessions.NewCookieStore(random.Bytes(32), random.Bytes(32)))
}

func NewSession(length int, name string, factory tokens.TokenFactory, store sessions.Store) tokens.TokenStorage {
	return Session{
		Length:      length,
		SessionName: name,
		Store:       store,
		Factory:     factory,
	}
}

type Session struct {
	// Length of the token to generate
	Length int

	// Name to store the token session
	// within the request as.
	SessionName string

	// Session storage
	sessions.Store

	// Factory for generating new tokens.
	// Should return pointers to a token.
	Factory tokens.TokenFactory
}

func (t Session) Load(r *http.Request) (tokens.Token, error) {
	// ignore session error, we always get a session back.
	// even if we failed to load a session from storage.
	session, _ := t.Store.Get(r, t.SessionName)

	if token, ok := session.Values[TOKEN_NAME].(tokens.Token); ok {
		return token, nil
	}

	return nil, tokens.ErrMissingToken
}

func (t Session) NewToken(w http.ResponseWriter, r *http.Request) tokens.Token {
	token := t.Factory(t.Length)
	// Ignore the error, Get always returns a session
	session, _ := t.Store.Get(r, t.SessionName)

	session.Values[TOKEN_NAME] = token

	if err := session.Save(r, w); err != nil {
		// if the save fails then the token we generated will
		// be invalid.
		return token
	}

	return token
}
